                        <li><!-- ========== portfolio item ========== -->
                    <div class="pic_hover">
                      <div class="title"><h4><?php print $fields['title']->content; ?></h4></div><!-- title -->
                      <a class="image" href="<?php print file_create_url($row->_field_data['nid']['entity']->field_home_popup_pic['und'][0]['uri']);?>" rel="prettyPhoto[gallery]">
                          <span class="rollover"></span>
                          <img src="<?php print image_style_url('portfolio', $row->_field_data['nid']['entity']->field_home_pic['und'][0]['uri']);?>" alt=""/><!-- main image -->
                      </a>
                    </div>
                    <div class="plus"></div><!-- maximize button -->
                    <div class="hide_content"><div class="inner">
                                <?php print $fields['body']->content; ?>
                    <a class="btn white" href="<?php print $fields['field_link_url']->content; ?>" target="_blank">View Website</a><!-- website link -->
                    <div class="clear"></div><!-- clear div -->
                    </div><!-- close inner -->
                    <div class="min"></div><!-- minimize button -->
                    </div><!-- close hidden content -->
                    <div class="line"></div><!-- bottom content -->
                        </li>

