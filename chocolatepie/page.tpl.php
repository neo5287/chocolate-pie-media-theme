<?php
?>

<div id="wrapper">
<div id="social-icons">
<?php print render($page['socialicons']); ?>
</div>
<!-- close div social-icons -->
	<div id="sidebar">
    	<!-- close div mainlogo -->
	<?php if ($logo) : ?>
    	<div id="mainlogo"><a href="<?php print $front_page ?>"><img src="<?php print $logo ?>" alt="" /></a></div>        
	<?php endif;?>

  <div id="mainmenu">
        <?php if (isset($main_menu)) : ?>
        <?php print theme('links__system_main_menu', array(
          'links' => $main_menu,
          'attributes' => array(
            'id' => 'main-menu-links',
            'class' => array('links', 'clearfix'),
          ),
          'heading' => array(
            'text' => t('Main menu'),
            'level' => 'h2',
            'class' => array('element-invisible'),
          ),
        )); ?>
        <?php endif; ?>
        </div><!-- close div mainmenu -->
    </div><!-- close div sidebar -->

    <div id="main">
    	<div id="header">
		<?php print render($page['header']); ?>
        </div>
        
        <div id="content">
		<?php if ($page['topcontent']): ?><?php print render($page['topcontent']);?><?php endif; ?>
          <?php //print $breadcrumb; ?>
		  
          <?php if ($tabs): ?><div id="tabs-wrapper" class="clearfix"><?php endif; ?>
          <?php print render($title_prefix); ?>
          <?php if ($title && !in_array(@$node->type, array('page','blog'))): ?>
            <h1<?php print $tabs ? ' class="with-tabs"' : '' ?>><?php print $title ?></h1>
          <?php endif; ?>
          <?php print render($title_suffix); ?>
          <?php if ($tabs): ?><?php print render($tabs); ?></div><?php endif; ?>
          <?php print render($tabs2); ?>
          <?php print $messages; ?>
          <?php print render($page['help']); ?>
          <?php if ($action_links && $_GET['q'] != "blog"): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
          <div class="clearfix">
            <?php print render($page['content']); ?>
          </div>
		  
</div><!-- close div content -->
	
		<div class="clear"></div>
		
		<div id="footer">
			<?php print render($page['footer']); ?>
		</div>
    </div><!-- close div main -->
</div><!-- close div wrapper -->
